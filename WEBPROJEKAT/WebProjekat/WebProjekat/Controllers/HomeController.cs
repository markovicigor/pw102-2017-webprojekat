﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;

namespace WebProjekat.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string searchString,DateTime? prviDatum,DateTime? drugiDatum,double? cena1,double? cena2,string sortOrder,string filterTip,string nerasprodate)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];

            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];

            manifestacije = manifestacije.OrderBy(x => x.DatumOdrzavanja).ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                ViewBag.SearchString = searchString;
                manifestacije = manifestacije.Where(m => m.Naziv.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) >= 0 || m.MestoOdrzavanja.Drzava.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) >= 0 || m.MestoOdrzavanja.Grad.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) >= 0 || m.MestoOdrzavanja.Ulica.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

            }
            if (prviDatum.ToString().Length != 0 &&  drugiDatum.ToString().Length != 0)
            {

                manifestacije = manifestacije.Where(x => x.DatumOdrzavanja >= prviDatum && x.DatumOdrzavanja <= drugiDatum).ToList();

            }
            if (cena1 != null && cena2 != null)
            {
                ViewBag.Cena1 = cena1;
                ViewBag.Cena2 = cena2;
                manifestacije = manifestacije.Where(x => x.CenaRegularKarte >= cena1 && x.CenaRegularKarte <= cena2).ToList();

            }

                switch (filterTip)
                {
                    case "FESTIVAL":
                    manifestacije = manifestacije.Where(m => m.Tip.ToString() == "FESTIVAL").ToList();
                    ViewBag.filterTip = "FESTIVAL";
                        break;
                    case "KONCERT":
                    manifestacije = manifestacije.Where(m => m.Tip.ToString() == "KONCERT").ToList();
                    ViewBag.filterTip = "KONCERT";
                    break;
                    case "POZORISTE":
                    manifestacije = manifestacije.Where(m => m.Tip.ToString() == "POZORISTE").ToList();
                    ViewBag.filterTip = "POZORISTE";
                    break;
                }
                if (!String.IsNullOrEmpty(nerasprodate))
                {
                    manifestacije = manifestacije.Where(m => m.BrojMesta > 0 && m.Status == StatusManifestacije.AKTIVNO).ToList();

                }

                switch (sortOrder)
                {

                    case "datum_desc":
                        manifestacije = manifestacije.OrderByDescending(m => m.DatumOdrzavanja).ToList();
                        ViewBag.SortOrder = "Datum(opadajuce)";
                        break;
                    case "Naziv":
                        manifestacije = manifestacije.OrderBy(m => m.Naziv).ToList();
                        ViewBag.SortOrder = "Naziv(A-Z)";
                        break;
                    case "naziv_desc":
                        manifestacije = manifestacije.OrderByDescending(m => m.Naziv).ToList();
                        ViewBag.SortOrder = "Naziv(Z-A)";
                        break;
                    case "Cena":
                        manifestacije = manifestacije.OrderBy(m => m.CenaRegularKarte).ToList();
                        ViewBag.SortOrder = "Cena(rastuce)";
                        break;
                    case "cena_desc":
                        manifestacije = manifestacije.OrderByDescending(m => m.CenaRegularKarte).ToList();
                        ViewBag.SortOrder = "Cena(opadajuce)";
                        break;
                    case "Mesto":
                        manifestacije = manifestacije.OrderBy(m => m.MestoOdrzavanja.Grad).ToList();
                        ViewBag.SortOrder = "Mesto/Grad(A-Z)";
                        break;
                    case "mesto_desc":
                        manifestacije = manifestacije.OrderByDescending(m => m.MestoOdrzavanja.Grad).ToList();
                        ViewBag.SortOrder = "Mesto/Grad(Z-A)";
                        break;

                }

            Korisnik korisnik = (Korisnik)Session["korisnik"];
            Korisnik administrator = (Korisnik)Session["administrator"];
            Korisnik prodavac = (Korisnik)Session["prodavac"];
            foreach (Manifestacija m in manifestacije)
            {
                Komentar komentar = komentari.Find(k => k.IdManifestacije == m.Id);
                if (komentar != null && komentar.Odobren == "ODOBREN")
                    m.Komentar = komentar;
                else
                {

                 m.Komentar.TekstKomentara = "";

                }

                if(administrator != null || prodavac != null)
                {
                    m.Komentari = komentari.FindAll(k => k.IdManifestacije == m.Id );
                }
                else
                {
                 m.Komentari = komentari.FindAll(k => k.IdManifestacije == m.Id && k.Odobren == "ODOBREN");
                }



            }
            ViewBag.komentari = komentari;


            if(korisnik != null)
            {
                ViewBag.korisnik = korisnik;
            }
            if(administrator != null)
            {
                ViewBag.administrator = administrator;
            }
            if(prodavac != null)
            {
                ViewBag.prodavac = prodavac;
            }
            if(administrator != null)
            {
                ViewBag.manifestacije = manifestacije;
            }
            else
            {
               // manifestacije = manifestacije.Where(x => x.Status == StatusManifestacije.AKTIVNO).ToList();
                ViewBag.manifestacije = manifestacije;

            }



            return View();
        }



    }
}