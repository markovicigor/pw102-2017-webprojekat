﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;

namespace WebProjekat.Controllers
{
    public class KartaController : Controller
    {
        // GET: Karta
        public ActionResult Index(string id)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            Manifestacija m = manifestacije.Find(x => x.Id == id);
            List<Manifestacija> noveManifestacije = new List<Manifestacija>();
            Korisnik korisnik = (Korisnik)Session["korisnik"];
            if (korisnik == null)
            {
                return RedirectToAction("Index", "Login");
            }

            if(m.Status == StatusManifestacije.NEAKTIVNO)
            {
                ViewBag.GreskaNeaktivnost = "Manifestacija nije aktivna";
                return View("Pomocni");
            }
            if (m.DatumOdrzavanja < DateTime.Now)
            {
                m.Status = StatusManifestacije.NEAKTIVNO;
                ViewBag.Greska = "Manifestacija je prosla";
                return View("Pomocni");
            }
            foreach (Manifestacija manifestacija in manifestacije.ToList())
            {
                noveManifestacije.Add(manifestacija);
            }
            Data.UpisiManifestacije(noveManifestacije);
            ViewBag.Manifestacija = m;
            ViewBag.korisnik = korisnik;
            return View();
        }
        public ActionResult Rezervacija(string id,TipKarte? tip,int? brojKarata)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            List<Korisnik> noviKorisnici = new List<Korisnik>();
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            Manifestacija m = manifestacije.Find(x => x.Id == id);
            double cena;
            if(tip == null)
            {
                ViewBag.GreskaTip = "Polje tip mora biti popunjeno";
                return View("Pomocni");
            }
            if(brojKarata < 1)
            {
                ViewBag.GreskaGreska = "Broj karata mora biti veci od 0";
                return View("Pomocni");
            }
            if(brojKarata == null)
            {
                ViewBag.GreskaBroj = "Polje broj karata mora biti popunjeno";
                return View("Pomocni");
            }
            if(m.BrojMesta < brojKarata)
            {
                ViewBag.Message3 = "Trazeni broj ulaznica nije dostupan";
                return View("Greska");
            }
            if(tip.ToString() == "REGULAR")
            {
                cena = m.CenaRegularKarte;
            }
            else if(tip.ToString() == "FANPIT")
            {
                cena = m.CenaRegularKarte * 2;
            }
            else if(tip.ToString() == "VIP")
            {
                cena = m.CenaRegularKarte * 4;
            }
            else
            {
                ViewBag.Message2 = "Morate izabrati vrstu karte.";
                return View("Pomocni");
            }

            Korisnik korisnik = (Korisnik)Session["korisnik"];
            if(korisnik == null)
            {
                return RedirectToAction("Index", "Login");
            }
            korisnik.Bodovi += cena / 1000 * 133;
            string tipKorisnika = korisnik.Tip.KojiJeTip(korisnik.Bodovi);
             if(tipKorisnika == "Bronzani")
            {
                korisnik.Tip.Popust = 1;
            }
            else if(tipKorisnika == "Srebrni")
            {
                korisnik.Tip.Popust = 0.97;
            }
            else if (tipKorisnika == "Zlatni")
            {
                korisnik.Tip.Popust = 0.95;
            }
            korisnik.Tip.ImeTipa = tipKorisnika;
            foreach (Korisnik k in korisnici.ToList())
            {
                noviKorisnici.Add(k);
            }
            Data.UpisiKorisnike(noviKorisnici);
            Karta karta = new Karta(m, m.DatumOdrzavanja, (double)(cena * brojKarata * korisnik.Tip.Popust), korisnik.Username,StatusKarte.ODUSTANAK);
            ViewBag.Username = korisnik.Username;
            ViewBag.Karta = karta;
            ViewBag.Tip = tip.ToString();
            ViewBag.BrojKarata = brojKarata;
            ViewBag.Status = karta.StatusKarte;
            return View("Rezervacija");
        }
        public ActionResult Kupovina(string id,string naziv,TipKarte? tip,double cena,StatusKarte st,int brojKarata)
        {

            List<Karta> karte = (List<Karta>)HttpContext.Application["karte"];
            List<Manifestacija> noveManifestacije = new List<Manifestacija>();
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            Manifestacija m = manifestacije.Find(x => x.Naziv == naziv);
            Korisnik korisnik = (Korisnik)Session["korisnik"];

            if (tip.ToString() == "VIP")
            {
                m.BrojVip -= brojKarata;
            }
            else if (tip.ToString() == "REGULAR")
            {
                m.BrojRegular -= brojKarata;
            }
            else if (tip.ToString() == "FANPIT")
            {
                m.BrojFanpit -= brojKarata;
            }
            m.BrojMesta = m.BrojVip + m.BrojRegular + m.BrojFanpit;
            if(m.BrojMesta <= 0)
            {
                m.Status = StatusManifestacije.NEAKTIVNO;
            }
            st = StatusKarte.REZERVISANA;
            foreach(Manifestacija manifestacija in manifestacije.ToList())
            {
                noveManifestacije.Add(manifestacija);
            }
            Data.UpisiManifestacije(noveManifestacije);

            ViewBag.Message = "Uspesno ste izvrsili rezervaciju karte.";

            Karta karta = new Karta(id,korisnik.Username,naziv,m.DatumOdrzavanja,(TipKarte)tip,cena,StatusKarte.REZERVISANA);
            karta.DatumRezervacije = DateTime.Now;
            karte.Add(karta);
            Data.Savekarta(id,naziv,m.DatumOdrzavanja,tip.ToString(),cena,korisnik.Username,st,false,m.Id);
        return View("Kupovina");
        }
        public ActionResult Odustanak(string id)
        {
            List<Karta> karte = (List<Karta>)HttpContext.Application["karte"];
            List<Karta> noveKarte = new List<Karta>();
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            List<Korisnik> noviKorisnici = new List<Korisnik>();
            Korisnik korisnik = (Korisnik)Session["korisnik"];
            Karta karta = karte.Find(x => x.Identifikator == id);

            if((karta.DatumManifestacije - karta.DatumRezervacije).TotalDays < 7)
            {
                ViewBag.Message = "Kartu nije moguce otkazati u zadnjih 7 dana pred manifestaciju!";
                return View("Pomocni");
            }
            karta.StatusKarte = StatusKarte.ODUSTANAK;

            foreach(Karta k in karte.ToList())
            {
                noveKarte.Add(k);
            }
            korisnik.Bodovi -= karta.Cena / 1000 * 133 * 4;
            string tipKorisnika = korisnik.Tip.KojiJeTip(korisnik.Bodovi);
            korisnik.Tip.ImeTipa = tipKorisnika;
            foreach (Korisnik k in korisnici.ToList())
            {
                noviKorisnici.Add(k);
            }
            Data.UpisiKorisnike(noviKorisnici);
            Data.UpisiKartu(noveKarte);
            ViewBag.Message4 = "Uspesno ste otkazali rezervaciju";
            return View("Pomocni");

        }
    }

}