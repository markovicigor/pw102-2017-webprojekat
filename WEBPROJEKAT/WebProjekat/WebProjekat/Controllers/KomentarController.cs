﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;

namespace WebProjekat.Controllers
{
    public class KomentarController : Controller
    {
        // GET: Komentar
        public ActionResult Index(string id,string nazivManifestacije)
        {
            List<Karta> karte = (List<Karta>)HttpContext.Application["karte"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            ViewBag.Id = id;
            ViewBag.NazivManifestacije = nazivManifestacije;
            Karta karta = karte.Find(x => x.Identifikator == id && x.DatumManifestacije < DateTime.Now);
            if(karta != null)
            {
                ViewBag.karta = karta;
                return View();
            }
            else
            {
                ViewBag.GreskaIstek = "Komentare mozete pisati samo kad manifestacija prodje!";
                return View("Greska");
            }


        }
        public ActionResult Komentar(string id,string tekstKomentara,int? ocena,string nazivManifestacije)
        {
            List<Karta> karte = (List<Karta>)HttpContext.Application["karte"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];
            Manifestacija m = manifestacije.Find(x => x.Naziv == nazivManifestacije);



            Karta karta = karte.Find(x => x.Identifikator == id);

            if(tekstKomentara.Length == 0)
            {
                ViewBag.GreskaString = "Polje tekst komentara mora biti popunjeno!";
                return View("Greska");
            }
            if(ocena == null)
            {
                ViewBag.GreskaOcena = "Polje ocena mora biti popunjeno!";
                return View("Greska");
            }
            if(ocena < 1 || ocena > 5)
            {
                ViewBag.GreskaKombo = "Polje ocena mora biti u rasponu od 1 do 5";
                return View("Greska");
            }

            Komentar komentar = new Komentar(karta.Kupac.Username, m.Id, tekstKomentara, (int)ocena);
            m.Komentari.Add(komentar);
            komentari.Add(komentar);
            Data.SaveKomentar(komentar);
            return RedirectToAction("Index", "Home", komentari);

        }
        public ActionResult Odobri(string id)
        {
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];
            List<Komentar> noviKomentari = new List<Komentar>();

            foreach(Komentar k in komentari.ToList())
            {
                if(k.Odobren == "NEODOBREN" && k.IdManifestacije == id)
                {
                    k.Odobren = "ODOBREN";

                }
                noviKomentari.Add(k);
            }
            Data.UpisiKomentar(noviKomentari);
            return RedirectToAction("Index", "Home");



        }
        public ActionResult PrikaziKomentare()
        {
            Korisnik prodavac = (Korisnik)Session["prodavac"];
            Korisnik admin = (Korisnik)Session["administrator"];
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];
            List<Komentar> noviKomentari = new List<Komentar>();
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];

            if(prodavac != null)
            manifestacije = manifestacije.Where(x => x.UsernameProdavca == prodavac.Username).ToList();

            foreach(Manifestacija m in manifestacije)
            {
                foreach(Komentar k in komentari)
                {
                    if(k.IdManifestacije == m.Id)
                    {
                        noviKomentari.Add(k);
                    }
                }
            }




            if (noviKomentari.Count > 0)
            {
                foreach (Manifestacija m in manifestacije)
                {
                    foreach (Komentar k in noviKomentari)
                    {
                        if (k.IdManifestacije == m.Id)
                        {
                            ViewBag.naziv = m.Naziv;
                        }
                    }
                }
                //noviKomentari = noviKomentari.Where(x => x.Odobren == "NEODOBREN").ToList();
            }


            ViewBag.prodavac = prodavac;
            ViewBag.admin = admin;

            ViewBag.Komentari = noviKomentari;
            ViewBag.AdminKomentari = komentari;

            return View("PrikaziKomentare");
        }

    }
}