﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;

namespace WebProjekat.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];

            Korisnik korisnik = korisnici.Find(u => u.Username == username && u.Password == password);

            if (korisnik == null)
            {
                ViewBag.Message = $"Korisnik sa ovim podacima ne postoji!";
                return View("Index");
            }


            if (!korisnik.Deleted)
            {

                if (korisnik.Uloga.ToString() == "KUPAC")
                {
                    Session["korisnik"] = korisnik;
                    korisnik.LoggedIn = true;
                }
                else if (korisnik.Uloga.ToString() == "ADMINISTRATOR")
                {
                    Session["administrator"] = korisnik;
                    korisnik.LoggedIn = true;
                }
                else if (korisnik.Uloga.ToString() == "PRODAVAC")
                {
                    Session["prodavac"] = korisnik;
                    korisnik.LoggedIn = true;
                }

            }
            else
            {
                ViewBag.Message = $"Korisnik je logicki obrisan";
                return View("Index");
            }

            return RedirectToAction("Index", "Home");


        }
        public ActionResult Logout()
        {
            Session["korisnik"] = null;
            Session["administrator"] = null;
            Session["prodavac"] = null;



            return View("Index");

        }
    }
}