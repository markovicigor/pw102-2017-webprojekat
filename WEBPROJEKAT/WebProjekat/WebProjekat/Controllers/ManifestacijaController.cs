﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;

namespace WebProjekat.Controllers
{
    public class ManifestacijaController : Controller
    {
        // GET: Manifestacija
        public ActionResult Index()
        {
            Korisnik prodavac = (Korisnik)Session["prodavac"];

            ViewBag.username = prodavac.Username;


            return View();

        }


        [HttpPost]
        public ActionResult Manifestacija(string id, string naziv, TipManifestacije? tip, DateTime? datumOdrzavanja, double? cenaRegularKarte, string ulica, int? brojUlice, string drzava, string grad, int? postanskiBroj, int? brojVip, int? brojRegular, int? brojFanpit,string usernameProdavca, HttpPostedFileBase file)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];


            if (id.Length ==0)
            {
                ViewBag.Greska10 = "Polje id mora biti popunjeno!";
                return View("Index");
            }
            if (naziv.Length ==0)
            {
                ViewBag.Greska11 = "Polje naziv mora biti popunjeno!";
                return View("Index");
            }
            if (tip == null)
            {
                ViewBag.Greska15 = "Polje tip mora biti popunjeno!";
                return View("Index");
            }
            if (datumOdrzavanja == null)
            {
               ViewBag.Greska12 = "Polje datum mora biti popunjeno!";
                return View("Index");
            }
            if (cenaRegularKarte == null)
            {
                ViewBag.Greska16 = "Polje cena mora biti popunjeno!";
                return View("Index");
            }
            if (cenaRegularKarte < 0)
            {
                ViewBag.GreskaCena = "Polje cena mora biti pozitivno!";
                return View("Index");
            }
            if (ulica.Length == 0)
            {
                ViewBag.Greska17 = "Polje ulica mora biti popunjeno!";
                return View("Index");
            }
            if (brojUlice == null)
            {
                ViewBag.Greska18 = "Polje broj ulice mora biti popunjeno!";
                return View("Index");
            }
            if (brojUlice < 0)
            {
                ViewBag.GreskaBrojUlice = "Polje broj ulice mora biti vece od 0!";
                return View("Index");
            }
            if (drzava.Length == 0)
            {
                ViewBag.Greska13 = "Polje drzava mora biti popunjeno!";
                return View("Index");
            }
            if (grad.Length == 0)
            {
                ViewBag.Greska14 = "Polje grad mora biti popunjeno!";
                return View("Index");

            }
            if (postanskiBroj == null)
            {
                ViewBag.Greska19 = "Polje postanski broj  mora biti popunjeno!";
                return View("Index");

            }
            if (postanskiBroj <= 0)
            {
                ViewBag.GreskaPB = "Polje postanski broj  mora biti vece od 0!";
                return View("Index");

            }
            if (brojVip == null)
            {
                ViewBag.Greska20 = "Polje broj vip karata mora biti popunjeno!";
                return View("Index");
            }
            if (brojVip < 0)
            {
                ViewBag.GreskaVip = "Polje broj vip karata ne moze biti negativno!";
                return View("Index");
            }
            if (brojRegular == null)
            {
                ViewBag.Greska21 = "Polje broj regular karata mora biti popunjeno!";
                return View("Index");
            }
            if (brojRegular < 0)
            {
                ViewBag.GreskaRegular = "Polje broj regular karata ne moze biti negativno!";
                return View("Index");
            }
            if (brojFanpit == null)
            {
                ViewBag.Greska22 = "Polje broj fanpit mora biti popunjeno!";
                return View("Index");
            }
            if (brojFanpit < 0)
            {
                ViewBag.GreskaFanpit = "Polje broj fanpit karata ne moze biti negativno!";
                return View("Index");
            }
            if (file == null)
            {
                ViewBag.Greska23 = "Morate da odaberete sliku";
                return View("Index");
            }
            if (manifestacije.Find(x => x.Id == id) != null)
            {
                ViewBag.Message2 = "Id mora biti unikatan";
                return View("Index");
            }
            if (manifestacije.Find(m => m.DatumOdrzavanja == datumOdrzavanja || (m.MestoOdrzavanja.Grad == grad && m.MestoOdrzavanja.Ulica == ulica && m.MestoOdrzavanja.Drzava == drzava && m.MestoOdrzavanja.BrojUlice == brojUlice)) != null)
            {
                ViewBag.Message = "Postoji vec manifestacija sa istim vremenom/lokacijom odrzavanja";
                return View("Index");
            }
            byte[] bajtovi = new byte[file.ContentLength];

            using (BinaryReader theReader = new BinaryReader(file.InputStream))
            {
                bajtovi = theReader.ReadBytes(file.ContentLength);
            }
            string file64 = Convert.ToBase64String(bajtovi);


             Manifestacija manifestacija = new Manifestacija(id, naziv, (TipManifestacije)tip, (int)(brojVip + brojRegular + brojFanpit), (DateTime)datumOdrzavanja, (double)cenaRegularKarte, ulica, (int)brojUlice, drzava, grad, (int)postanskiBroj, (int)brojVip, (int)brojRegular, (int)brojFanpit, usernameProdavca,file64);

            manifestacije.Add(manifestacija);

            Data.SaveManifestacija(manifestacija);
            Session["manifestacija"] = manifestacija;
            return RedirectToAction("Index", "Home");

        }
        public ActionResult Prikazi(string id)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            Manifestacija m = manifestacije.Find(x => x.Id == id);
            List<Manifestacija> noveManifestacije = new List<Manifestacija>();
            Korisnik korisnik = (Korisnik)Session["korisnik"];
            Korisnik administrator = (Korisnik)Session["administrator"];
            Korisnik prodavac = (Korisnik)Session["prodavac"];



            if (korisnik != null)
            {
                ViewBag.korisnik = korisnik;
            }
            if (administrator != null)
            {
                ViewBag.administrator = administrator;
            }
            if (prodavac != null)
            {
                ViewBag.prodavac = prodavac;
            }

            ViewBag.Manifestacija = m;
            ViewBag.Slika = m.Slika;
            return View("Prikazi");
        }
        public ActionResult Edit(string id)
        {
            ViewBag.id = id;
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            Manifestacija m = manifestacije.Find(x => x.Id == id);
            Korisnik prodavac = (Korisnik)Session["prodavac"];
            if (m.UsernameProdavca == prodavac.Username)
            {
                ViewBag.Naziv = m.Naziv;
                ViewBag.BrojVip = m.BrojVip;
                ViewBag.BrojRegular = m.BrojRegular;
                ViewBag.BrojFanpit = m.BrojFanpit;
                ViewBag.CenaRegularKarte = m.CenaRegularKarte;
                ViewBag.Ulica = m.MestoOdrzavanja.Ulica;
                ViewBag.BrojUlice = m.MestoOdrzavanja.BrojUlice;
                ViewBag.Drzava = m.MestoOdrzavanja.Drzava;
                ViewBag.Grad = m.MestoOdrzavanja.Grad;
                ViewBag.PostanskiBroj = m.MestoOdrzavanja.PostanskiBroj;
            }
            else
            {
                ViewBag.Greska2 = "Mozete da menjate samo svoje manifestacije!";
                return View("Greska");
            }
            return View("Edit");
        }

        public ActionResult Editt(string id, string naziv, TipManifestacije? tip, DateTime? datumOdrzavanja, double? cenaRegularKarte, string ulica, int? brojUlice, string drzava, string grad, int? postanskiBroj, int? brojVip, int? brojRegular,int? brojFanpit)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Manifestacija> noveManifestacije = new List<Manifestacija>();
            List<Karta> karte = (List<Karta>)HttpContext.Application["karte"];
            List<Karta> noveKarte = new List<Karta>();
            Korisnik prodavac = (Korisnik)Session["prodavac"];
            foreach (Manifestacija m in manifestacije.ToList())
            {
                if (m.Id == id )
                {

                    if (naziv != "")
                        m.Naziv = naziv;

                    if (tip.ToString().Length != 0)
                        m.Tip = (TipManifestacije)tip;
                    if (datumOdrzavanja.ToString() != "")
                        m.DatumOdrzavanja = (DateTime)datumOdrzavanja;
                    if (cenaRegularKarte != null)
                        m.CenaRegularKarte= (double)cenaRegularKarte;
                    if (ulica != "")
                        m.MestoOdrzavanja.Ulica = ulica ;
                    if (brojUlice != null)
                        m.MestoOdrzavanja.BrojUlice = (int)brojUlice;
                    if (drzava != "")
                        m.MestoOdrzavanja.Drzava = drzava;
                    if (grad  != "")
                        m.MestoOdrzavanja.Grad = grad;
                    if (postanskiBroj != null)
                        m.MestoOdrzavanja.PostanskiBroj = (int)postanskiBroj;
                    if (brojVip != null)
                        m.BrojVip = (int)brojVip;
                    if (brojRegular != null)
                        m.BrojRegular = (int)brojRegular;
                    if (brojFanpit != null)
                        m.BrojFanpit = (int)brojFanpit;
                        m.BrojMesta = m.BrojVip + m.BrojRegular + m.BrojFanpit;

                    foreach (Karta k in karte.ToList())
                    {

                        if (k.IdManifestacije == m.Id)
                        {
                            k.Manifestacija = new Manifestacija();
                            k.Manifestacija = m;
                            k.DatumManifestacije = m.DatumOdrzavanja;
                            k.Cena = m.CenaRegularKarte;
                            k.NazivManifestacije = m.Naziv;

                        }

                    }
                    foreach(Karta k in karte.ToList())
                    {
                        noveKarte.Add(k);
                    }

                }

            }
            foreach (Manifestacija m in manifestacije.ToList())
            {
                noveManifestacije.Add(m);
            }
            Data.UpisiKartu(noveKarte);
            Data.UpisiManifestacije(noveManifestacije);
            return RedirectToAction("Index", "Home");
        }
        public ActionResult Delete(string id)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Manifestacija> noveManifestacije = new List<Manifestacija>();
            List<Karta> karte = (List<Karta>)HttpContext.Application["karte"];
            List<Karta> noveKarte = new List<Karta>();
            foreach (Manifestacija m in manifestacije.ToList())
            {
                if (m.Id == id)
                {
                    m.Deleted = true;
                    m.Status = StatusManifestacije.NEAKTIVNO;
                    ViewBag.Obrisan = m;

                }

            }
            Manifestacija manifestacija = manifestacije.Find(x => x.Id == id);
            foreach (Karta k in karte.ToList())
            {
                if (k.NazivManifestacije == manifestacija.Naziv)
                {
                    k.Deleted = true;
                    k.IdManifestacije = manifestacija.Id;
                }

            }
            foreach (Karta k in karte.ToList())
            {
                noveKarte.Add(k);

            }
            foreach (Manifestacija m in manifestacije.ToList())
            {
                noveManifestacije.Add(m);
            }


            ViewBag.manifestacije = noveManifestacije;
            Data.UpisiKartu(noveKarte);
            Data.UpisiManifestacije(noveManifestacije);
            return RedirectToAction("Index", "Home");
        }
        public ActionResult Odobri(string id)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Manifestacija> noveManifestacije = new List<Manifestacija>();

            foreach (Manifestacija m in manifestacije.ToList())
            {
                if (m.Status.ToString() == "NEAKTIVNO" && m.Id == id)
                {
                    m.Status = StatusManifestacije.AKTIVNO;

                }
                noveManifestacije.Add(m);
            }
            Data.UpisiManifestacije(noveManifestacije);
            return RedirectToAction("Index", "Home");



        }
        public ActionResult MojeManifestacije(string searchString, DateTime? prviDatum, DateTime? drugiDatum, double? cena1, double? cena2, string sortOrder, string filterTip, string nerasprodate)
        {
            Korisnik prodavac = (Korisnik)Session["prodavac"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Manifestacija> noveManifestacije = new List<Manifestacija>();

            foreach (Manifestacija m in manifestacije.ToList())
            {
                if (m.UsernameProdavca == prodavac.Username)
                {
                   noveManifestacije.Add(m);
                }

            }
            noveManifestacije = noveManifestacije.OrderBy(x => x.DatumOdrzavanja).ToList();

            ViewBag.DatumSortParm = String.IsNullOrEmpty(sortOrder) ? "datum_desc" : "";
            ViewBag.NazivSortParm = sortOrder == "Naziv" ? "naziv_desc" : "Naziv";
            ViewBag.CenaSortParm = sortOrder == "Cena" ? "cena_desc" : "Cena";
            ViewBag.MestoSortParm = sortOrder == "MestoOdrzavanja" ? "mesto_desc" : "Mesto";


            if (!String.IsNullOrEmpty(searchString))
            {
                noveManifestacije = noveManifestacije.Where(m => m.Naziv.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) >= 0 || m.MestoOdrzavanja.Drzava.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) >= 0 || m.MestoOdrzavanja.Grad.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) >= 0 || m.MestoOdrzavanja.Ulica.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

            }
            if (prviDatum.ToString().Length != 0 && drugiDatum.ToString().Length != 0)
            {
                noveManifestacije = noveManifestacije.Where(x => x.DatumOdrzavanja >= prviDatum && x.DatumOdrzavanja <= drugiDatum).ToList();
            }
            if (cena1 != null && cena2 != null)
            {
                noveManifestacije = noveManifestacije.Where(x => x.CenaRegularKarte >= cena1 && x.CenaRegularKarte <= cena2).ToList();
            }
            switch (filterTip)
            {
                case "FESTIVAL":
                    noveManifestacije = noveManifestacije.Where(m => m.Tip.ToString() == "FESTIVAL").ToList();
                    break;
                case "KONCERT":
                    noveManifestacije = noveManifestacije.Where(m => m.Tip.ToString() == "KONCERT").ToList();
                    break;
                case "POZORISTE":
                    noveManifestacije = noveManifestacije.Where(m => m.Tip.ToString() == "POZORISTE").ToList();
                    break;
            }
            if (!String.IsNullOrEmpty(nerasprodate))
            {
                noveManifestacije = noveManifestacije.Where(m => m.BrojMesta > 0).ToList();

            }

            switch (sortOrder)
            {
                case "datum_desc":
                    noveManifestacije = noveManifestacije.OrderByDescending(m => m.DatumOdrzavanja).ToList();
                    break;
                case "Naziv":
                    noveManifestacije = noveManifestacije.OrderBy(m => m.Naziv).ToList();
                    break;
                case "naziv_desc":
                    noveManifestacije = noveManifestacije.OrderByDescending(m => m.Naziv).ToList();
                    break;
                case "Cena":
                    noveManifestacije = noveManifestacije.OrderBy(m => m.CenaRegularKarte).ToList();
                    break;
                case "cena_desc":
                    noveManifestacije = noveManifestacije.OrderByDescending(m => m.CenaRegularKarte).ToList();
                    break;
                case "Mesto":
                    noveManifestacije = noveManifestacije.OrderBy(m => m.MestoOdrzavanja.Grad).ToList();
                    break;
                case "mesto_desc":
                    noveManifestacije = noveManifestacije.OrderByDescending(m => m.MestoOdrzavanja.Grad).ToList();
                    break;

            }
            ViewBag.manifestacije = noveManifestacije;
            return View("MojeManifestacije");
        }
    }

}