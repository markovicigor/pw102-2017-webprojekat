﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;

namespace WebProjekat.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Register
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Register()
        {
            Korisnik korisnik = new Korisnik();
            Session["korisnik"] = korisnik;
            if(Session["administrator"]!= null)
            {
                Session["prodavac"] = korisnik;
            }
            return View(korisnik);
        }
        [HttpPost]

        public ActionResult Register(Korisnik korisnik)
        {

            Korisnik administrator = (Korisnik)Session["administrator"];
            Korisnik prodavac = (Korisnik)Session["prodavac"];
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            if (korisnici.Contains(korisnik))
            {
                ViewBag.Message = $"Korisnik sa  {korisnik.Username} vec postoji!";
                return View("Index");
            }

            if (korisnik.Username == null)
            {
                ViewBag.Message1 = "Korisnicko ime mora biti popunjeno!";
                return View("Index");
            }
            if (korisnik.Username.Length < 3)
            {
                ViewBag.Message2 = "Korisnicko ime mora sadrzati najmanje 3 karaktera!";
                return View("Index");
            }
            if (korisnik.Password == null)
            {
                ViewBag.Message3 = "Lozinka  mora biti popunjena!";
                return View("Index");
            }
            if (korisnik.Password.Length < 8)
            {
                ViewBag.Message4 = "Lozinka  mora imati najmanje 8 karaktera!";
                return View("Index");
            }
            if (!Regex.IsMatch(korisnik.Password, @"^[a-zA-Z0-9]+$"))
            {
                ViewBag.Message5 = "Lozinka  mora sadrzati samo slova ili brojeve!";
                return View("Index");

            }
            if (korisnik.Ime == null)
            {
                ViewBag.Message6 = "Polje mora biti popunjeno!";
                return View("Index");
            }
            if (korisnik.Prezime == null)
            {
                ViewBag.Message7 = "Polje prezime biti popunjeno!";
                return View("Index");
            }

            if (administrator != null)
            {

                korisnik.Uloga = Uloga.PRODAVAC;
                korisnik.Tip.ImeTipa = "/";
                korisnici.Add(korisnik);
                Data.SaveUser(korisnik);

                return RedirectToAction("Index", "Home");
            }
            else
            {
                korisnici.Add(korisnik);
                Data.SaveUser(korisnik);



            }

            return RedirectToAction("Index", "Login");
        }
    }
}