﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;

namespace WebProjekat.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        public ActionResult Index(string searchString, string sortOrder, string filterUloga, string filterTip)
        {
            Korisnik k = (Korisnik)Session["korisnik"];
            Korisnik administrator = (Korisnik)Session["administrator"];
            Korisnik prodavac = (Korisnik)Session["prodavac"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Manifestacija> noveManifestacije = new List<Manifestacija>();
            List<Karta> karte = (List<Karta>)HttpContext.Application["karte"];
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            List<Karta> noveKarte = new List<Karta>();
            List<Korisnik> noviKorisnici = new List<Korisnik>();


            if (prodavac != null)
            {
                foreach (Manifestacija m in manifestacije.ToList())
                {
                    if (m.UsernameProdavca == prodavac.Username)
                    {
                        noveManifestacije.Add(m);
                    }

                }
            }

            foreach (Manifestacija m in noveManifestacije.ToList())
            {
                foreach(Karta karta in karte)
                {
                    if(m.Naziv == karta.NazivManifestacije && karta.StatusKarte == StatusKarte.REZERVISANA)
                    {
                        noveKarte.Add(karta);
                    }


                }

            }
            ViewBag.Karte = noveKarte;
           foreach(Karta karta in noveKarte)
            {
                foreach (Korisnik korisnik in korisnici.ToList())
                {
                    if(karta.Kupac.Username == korisnik.Username && karta.StatusKarte == StatusKarte.REZERVISANA)
                    {
                        noviKorisnici.Add(korisnik);
                    }
                }
            }
            ViewBag.prodavacKorisnici = noviKorisnici;

            if (!String.IsNullOrEmpty(searchString))
            {
                korisnici = korisnici.Where(m => m.Ime.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) >= 0 || m.Prezime.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) >= 0 || m.Username.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

            }
            ViewBag.ImeSortParm = String.IsNullOrEmpty(sortOrder) ? "ime_desc" : "";
            ViewBag.PrezimeSortParm = sortOrder == "Prezime" ? "prezime_desc" : "Prezime";
            ViewBag.BodoviSortParm = sortOrder == "Bodovi" ? "bodovi_desc" : "Bodovi";
            ViewBag.UsernameSortParm = sortOrder == "Username" ? "username_desc" : "Username";
            switch (sortOrder)
            {
                case "Ime":
                 korisnici = korisnici.OrderBy(m => m.Ime).ToList();
                    break;
                case "ime_desc":
                    korisnici = korisnici.OrderByDescending(m => m.Ime).ToList();
                    break;
                case "Prezime":
                    korisnici = korisnici.OrderBy(m => m.Prezime).ToList();
                    break;
                case "prezime_desc":
                    korisnici = korisnici.OrderByDescending(m => m.Prezime).ToList();
                    break;
                case "Bodovi":
                    korisnici = korisnici.OrderBy(m => m.Bodovi).ToList();
                    break;
                case "bodovi_desc":
                    korisnici = korisnici.OrderByDescending(m => m.Bodovi).ToList();
                    break;
                case "Username":
                    korisnici = korisnici.OrderBy(m => m.Username).ToList();
                    break;
                case "username_desc":
                    korisnici = korisnici.OrderByDescending(m => m.Username).ToList();
                    break;

            }
            switch (filterUloga)
            {
                case "ADMINISTRATOR":
                    korisnici = korisnici.Where(m => m.Uloga.ToString() == "ADMINISTRATOR").ToList();
                    break;
                case "PRODAVAC":
                    korisnici = korisnici.Where(m => m.Uloga.ToString() == "PRODAVAC").ToList();
                    break;
                case "KUPAC":
                    korisnici = korisnici.Where(m => m.Uloga.ToString() == "KUPAC").ToList();
                    break;
            }
            switch (filterTip)
            {
                case "Zlatni":
                    korisnici = korisnici.Where(m => m.Tip.ImeTipa == "Zlatni").ToList();
                    break;
                case "Srebrni":
                    korisnici = korisnici.Where(m => m.Tip.ImeTipa == "Srebrni").ToList();
                    break;
                case "Bronzani":
                    korisnici = korisnici.Where(m => m.Tip.ImeTipa == "Bronzani").ToList();
                    break;
            }

            if (k != null)
            {
                ViewBag.korisnik = k;
                ViewBag.kupac = k;
            }
            if (administrator != null)
            {
                ViewBag.korisnik = administrator;
            }
            if (prodavac != null)
            {
                ViewBag.korisnik = prodavac;
                ViewBag.prodavac = prodavac;
            }
            if (administrator != null)
            {
                ViewBag.administrator = administrator;
            }
            ViewBag.korisnici = korisnici;
            return View();
        }
        public ActionResult Edit(string username)
        {
            ViewBag.username = username;

            return View("Edit");
        }

        public ActionResult Editt(string username, string password, string ime ,string prezime, Pol? pol, DateTime? datumRodjenja)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            List<Korisnik> noviKorisnici = new List<Korisnik>();
            Korisnik k = (Korisnik)Session["korisnik"];
            Korisnik p = (Korisnik)Session["prodavac"];
            Korisnik a = (Korisnik)Session["administrator"];


            if(k != null)
             {
                  if (password != "")
                  {
                    if(password.Length < 8)
                    {
                        ViewBag.Greska10 = "Polje password mora biti duze od 8 znakova";
                        return View("Edit");
                    }
                    if (!Regex.IsMatch(password, @"^[a-zA-Z0-9]+$"))
                    {
                        ViewBag.Greska11 = "Lozinka  mora sadrzati samo slova ili brojeve!";
                        return View("Edit");

                    }
                    k.Password = password;
                  }


                   if (ime != "")
                   k.Ime = ime;
                    if (prezime != "")
                   k.Prezime = prezime;
                   if(pol.ToString().Length != 0)
                    k.Pol = (Pol)pol;
                   if(datumRodjenja.ToString().Length != 0)
                    k.DatumRodjenja = (DateTime)datumRodjenja;
            }
            else if (p != null)
            {
                if (password != "")
                {
                    if (password.Length < 8)
                    {
                        ViewBag.Greska10 = "Polje password mora biti duze od 8 znakova";
                        return View("Edit");
                    }
                    if (!Regex.IsMatch(password, @"^[a-zA-Z0-9]+$"))
                    {
                        ViewBag.Greska11 = "Lozinka  mora sadrzati samo slova ili brojeve!";
                        return View("Edit");

                    }
                    p.Password = password;
                }

                if (ime != "")
                    p.Ime = ime;
                if (prezime != "")
                    p.Prezime = prezime;
                if (pol.ToString().Length != 0)
                    p.Pol = (Pol)pol;
                if (datumRodjenja.ToString().Length != 0)
                    p.DatumRodjenja = (DateTime)datumRodjenja;
            }
            else if (a != null)
            {
                if (password != "")
                {
                    if (password.Length < 8)
                    {
                        ViewBag.Greska10 = "Polje password mora biti duze od 8 znakova";
                        return View("Edit");
                    }
                    if (!Regex.IsMatch(password, @"^[a-zA-Z0-9]+$"))
                    {
                        ViewBag.Greska11 = "Lozinka  mora sadrzati samo slova ili brojeve!";
                        return View("Edit");

                    }
                    a.Password = password;
                }

                if (ime != "")
                    a.Ime = ime;
                if (prezime != "")
                    a.Prezime = prezime;
                if (pol.ToString().Length != 0)
                    a.Pol = (Pol)pol;
                if (datumRodjenja.ToString().Length != 0)
                    a.DatumRodjenja = (DateTime)datumRodjenja;
            }
            else
            {
                ViewBag.Message = "Molimo da se ulogujete prvo!";
                return RedirectToAction("Index", "Login");

            }




            foreach (Korisnik korisnik in korisnici.ToList())
            {
                noviKorisnici.Add(korisnik);
            }
            Data.UpisiKorisnike(noviKorisnici);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult PrikazKarata(string searchString,DateTime? prviDatum, DateTime? drugiDatum, double? cena1, double? cena2, string sortOrder, string filterTip,string status)
        {
            List<Karta> odredjeneKarte = (List<Karta>)HttpContext.Application["karte"];

            List<Karta> odredjeneKarte2 = new List<Karta>();

            Korisnik k = (Korisnik)Session["korisnik"];
            Korisnik administrator = (Korisnik)Session["administrator"];
            Korisnik prodavac = (Korisnik)Session["prodavac"];
            if (k != null)
            {

                ViewBag.kupac = k;
            }
            if (prodavac != null)
            {

                ViewBag.prodavac = prodavac;
            }
            if (administrator != null)
            {
                ViewBag.administrator = administrator;
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                odredjeneKarte = odredjeneKarte.Where(x => string.Equals(x.NazivManifestacije, searchString, StringComparison.OrdinalIgnoreCase)).ToList();

            }
            if (prviDatum.ToString().Length != 0 && drugiDatum.ToString().Length != 0)
            {
                odredjeneKarte = odredjeneKarte.Where(x => x.DatumManifestacije >= prviDatum && x.DatumManifestacije <= drugiDatum).ToList();
            }
            if (cena1 != null && cena2 != null)
            {
               odredjeneKarte = odredjeneKarte.Where(x => x.Cena >= cena1 && x.Cena <= cena2).ToList();
            }
            ViewBag.DatumSortParm = String.IsNullOrEmpty(sortOrder) ? "datum_desc" : "";
            ViewBag.NazivSortParm = sortOrder == "Naziv" ? "naziv_desc" : "Naziv";
            ViewBag.CenaSortParm = sortOrder == "Cena" ? "cena_desc" : "Cena";
            switch (filterTip)
            {
                case "VIP":
                    odredjeneKarte = odredjeneKarte.Where(m => m.Tip.ToString() == "VIP").ToList();
                    break;
                case "REGULAR":
                    odredjeneKarte = odredjeneKarte.Where(m => m.Tip.ToString() == "REGULAR").ToList();
                    break;
                case "FANPIT":
                    odredjeneKarte = odredjeneKarte.Where(m => m.Tip.ToString() == "FANPIT").ToList();
                    break;
            }
            switch (status)
            {
                case "REZERVISANA":
                    odredjeneKarte = odredjeneKarte.Where(m => m.StatusKarte.ToString() == "REZERVISANA").ToList();
                    break;
                case "ODUSTANAK":
                    odredjeneKarte = odredjeneKarte.Where(m => m.StatusKarte.ToString() == "ODUSTANAK").ToList();
                    break;
            }
            switch (sortOrder)
            {
                case "datum_desc":
                    odredjeneKarte = odredjeneKarte.OrderByDescending(m => m.DatumManifestacije).ToList();
                    break;
                case "Naziv":
                    odredjeneKarte = odredjeneKarte.OrderBy(m => m.NazivManifestacije).ToList();
                    break;
                case "naziv_desc":
                    odredjeneKarte = odredjeneKarte.OrderByDescending(m => m.NazivManifestacije).ToList();
                    break;
                case "Cena":
                    odredjeneKarte = odredjeneKarte.OrderBy(m => m.Cena).ToList();
                    break;
                case "cena_desc":
                    odredjeneKarte = odredjeneKarte.OrderByDescending(m => m.Cena).ToList();
                    break;
            }
            if(k!=null)
            {
                odredjeneKarte2 = odredjeneKarte.FindAll(x => x.Kupac.Username == k.Username && x.StatusKarte == StatusKarte.REZERVISANA && x.Deleted == false);
            }

            if (odredjeneKarte2.Count > 0)
            {

                ViewBag.Username = k.Username;
            }
            ViewBag.AdminKarte = odredjeneKarte;
            ViewBag.OdredjeneKarte = odredjeneKarte2;
            return View("PrikazKarata");
        }
        public ActionResult Delete(string username)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            List<Korisnik> noviKorisnici = new List<Korisnik>();

            foreach (Korisnik k in korisnici.ToList())
            {
                if (k.Username == username)
                {

                    k.Deleted = true;
                    ViewBag.Message = $"Korisnik je obrisan logicki";
                    ViewBag.Obrisan = k;

                }

            }


            foreach (Korisnik k in korisnici.ToList())
            {
                noviKorisnici.Add(k);
            }
            Korisnik korisnik = (Korisnik)Session["korisnik"];
            Korisnik administrator = (Korisnik)Session["administrator"];
            Korisnik prodavac = (Korisnik)Session["prodavac"];

            if (korisnik != null)
            {
                ViewBag.korisnik = korisnik;
            }
            if (administrator != null)
            {
                ViewBag.korisnik = administrator;
            }
            if (prodavac != null)
            {
                ViewBag.korisnik = prodavac;
            }

            ViewBag.korisnici = noviKorisnici;


            if (administrator != null)
            {
                ViewBag.administrator = administrator;
            }
            Data.UpisiKorisnike(noviKorisnici);
            return View("Index");
        }
    }
}