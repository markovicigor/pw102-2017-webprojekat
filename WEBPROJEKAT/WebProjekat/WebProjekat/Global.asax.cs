﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebProjekat.Models;

namespace WebProjekat
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            List<Korisnik> korisnici = Data.ReadUsers("~/App_Data/korisnici.txt");
            HttpContext.Current.Application["korisnici"] = korisnici;

            List<Manifestacija> manifestacije = Data.ReadManifestacije("~/App_Data/manifestacije.txt");
            HttpContext.Current.Application["manifestacije"] = manifestacije;

            List<Manifestacija> pretragaManifestacije = new List<Manifestacija>();
            HttpContext.Current.Application["pretraga"] = pretragaManifestacije;

            List<Karta> karte = Data.ReadKarte("~/App_Data/karte.txt");
            HttpContext.Current.Application["karte"] = karte;

            List<Komentar> komentari = Data.ReadKomentari("~/App_Data/komentari.txt");
            HttpContext.Current.Application["komentari"] = komentari;

            Dictionary<string,string> slike = Data.ReadSlike("~/App_Data/slike.txt");
            HttpContext.Current.Application["slike"] = slike;

        }
    }
}
