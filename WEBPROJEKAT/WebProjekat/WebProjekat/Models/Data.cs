﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace WebProjekat.Models
{
    public static class Data
    {
        public static List<Korisnik> ReadUsers(string path)
        {
            List<Korisnik> users = new List<Korisnik>();

            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');
                Korisnik p = new Korisnik(tokens[0], tokens[1], tokens[2], tokens[3], ((Pol)Enum.Parse(typeof(Pol), tokens[4])), Convert.ToDateTime(tokens[5],null), ((Uloga)Enum.Parse(typeof(Uloga),tokens[6])),tokens[7],double.Parse(tokens[8]));

                p.Deleted = bool.Parse(tokens[9]);

                if (bool.Parse(tokens[9]))
                {
                    p.Deleted = true;
                }

                if (((Uloga)Enum.Parse(typeof(Uloga), tokens[6])).ToString() == "ADMINISTRATOR")
                {
                    p.Uloga = Uloga.ADMINISTRATOR;
                }
                if (((Uloga)Enum.Parse(typeof(Uloga), tokens[6])).ToString() == "PRODAVAC")
                {
                    p.Uloga = Uloga.PRODAVAC;
                }
                users.Add(p);
            }
            sr.Close();
            stream.Close();

            return users;
        }
        public static List<Karta> ReadKarte(string path)
        {
            List<Karta> karte = new List<Karta>();

            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');
                Karta k = new Karta(tokens[0], tokens[1], tokens[2], Convert.ToDateTime(tokens[3]), ((TipKarte)Enum.Parse(typeof(TipKarte), tokens[4])),int.Parse(tokens[5]), ((StatusKarte)Enum.Parse(typeof(StatusKarte), tokens[6])));
                k.DatumRezervacije = (Convert.ToDateTime(tokens[7]));
                k.Deleted = bool.Parse(tokens[8]);
                k.IdManifestacije = tokens[9];
                if (bool.Parse(tokens[8]))
                {
                    k.Deleted = true;
                }
                karte.Add(k);
            }
            sr.Close();
            stream.Close();

            return karte;
        }
        public static Dictionary<string, string> ReadSlike(string path)
        {
            Dictionary<string, string> slike = new Dictionary<string, string>();

            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');

                slike.Add(tokens[0], tokens[1]);

            }
            sr.Close();
            stream.Close();

            return slike;
        }
        public static List<Komentar> ReadKomentari(string path)
        {
            List<Komentar> komentari = new List<Komentar>();

            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');
                Komentar k = new Komentar(tokens[0], tokens[1], tokens[2], int.Parse(tokens[3]));

                k.Odobren = tokens[4];

                if(tokens[4] == "ODOBREN")
                {
                    k.Odobren = "ODOBREN";
                }
               komentari.Add(k);
            }
            sr.Close();
            stream.Close();

            return komentari;
        }

        public static void SaveSlika(string id,string file)
        {
            string putanja = HttpContext.Current.Server.MapPath(@"~/App_Data/slike.txt");
            TextWriter tw = new StreamWriter(putanja, true);



            tw.WriteLine(id + ";" + file);

            tw.Close();
        }
        public static List<Manifestacija> ReadManifestacije(string path)
        {
            List<Manifestacija> manifestacije = new List<Manifestacija>();

            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');
                Manifestacija m = new Manifestacija(tokens[0],tokens[1], ((TipManifestacije)Enum.Parse(typeof(TipManifestacije),tokens[2])),int.Parse(tokens[3]), Convert.ToDateTime(tokens[4], null), Double.Parse(tokens[5]), ((StatusManifestacije)Enum.Parse(typeof(StatusManifestacije), tokens[6])));

                m.MestoOdrzavanja.Ulica = tokens[7];
                m.MestoOdrzavanja.BrojUlice = int.Parse(tokens[8]);
                m.MestoOdrzavanja.Drzava = tokens[9];
                m.MestoOdrzavanja.Grad = tokens[10];
                m.MestoOdrzavanja.PostanskiBroj = int.Parse(tokens[11]);
                m.BrojVip = int.Parse(tokens[12]);
                m.BrojRegular = int.Parse(tokens[13]);
                m.BrojFanpit = int.Parse(tokens[14]);
                m.Deleted = bool.Parse(tokens[15]);
                m.UsernameProdavca = tokens[16];
                m.Slika = tokens[17];

                if (bool.Parse(tokens[15]))
                {
                    m.Deleted = true;
                }
                if (((StatusManifestacije)Enum.Parse(typeof(StatusManifestacije), tokens[6])).ToString() == "AKTIVNO")
                {
                    m.Status = StatusManifestacije.AKTIVNO;
                }

                manifestacije.Add(m);
            }
            sr.Close();
            stream.Close();

            return manifestacije;
        }
        public static void SaveUser(Korisnik korisnik)
        {
            string putanja = HttpContext.Current.Server.MapPath(@"~/App_Data/korisnici.txt");
            TextWriter tw = new StreamWriter(putanja, true);



            tw.WriteLine(korisnik.Username + ";" + korisnik.Password + ";" + korisnik.Ime + ";" + korisnik.Prezime + ";" + korisnik.Pol + ";" + korisnik.DatumRodjenja + ";" + korisnik.Uloga + ";" + korisnik.Tip.ImeTipa + ";" + korisnik.Bodovi + ";" + korisnik.Deleted );

            tw.Close();
        }
        public static void SaveKomentar(Komentar komentar)
        {
            string putanja = HttpContext.Current.Server.MapPath(@"~/App_Data/komentari.txt");
            TextWriter tw = new StreamWriter(putanja, true);



            tw.WriteLine(komentar.Username + ";" + komentar.IdManifestacije + ";" + komentar.TekstKomentara + ";" + komentar.Ocena + ";" + komentar.Odobren);

            tw.Close();
        }
        public static void Savekarta(string id,string naziv,DateTime datumOdrzavanja,string tip, double cena,string username, StatusKarte st,bool deleted,string idManifestacije)
        {
            string putanja = HttpContext.Current.Server.MapPath(@"~/App_Data/karte.txt");
            TextWriter tw = new StreamWriter(putanja, true);



            tw.WriteLine(id + ";" + username + ";" + naziv + ";" + datumOdrzavanja + ";" + tip + ";" + cena + ";" + st + ";" + DateTime.Now + ";" + deleted + ";" + idManifestacije);

            tw.Close();
        }
        public static void SaveManifestacija(Manifestacija manifestacija)
        {
            string putanja = HttpContext.Current.Server.MapPath(@"~/App_Data/manifestacije.txt");
            TextWriter tw = new StreamWriter(putanja, true);


            string str1 = manifestacija.Id + ";" + manifestacija.Naziv + ";" + manifestacija.Tip + ";" + manifestacija.BrojMesta + ";"+ manifestacija.DatumOdrzavanja + ";" + manifestacija.CenaRegularKarte + ";" + manifestacija.Status;

            string str2 = manifestacija.MestoOdrzavanja.Ulica + ";" + manifestacija.MestoOdrzavanja.BrojUlice + ";" + manifestacija.MestoOdrzavanja.Drzava + ";" + manifestacija.MestoOdrzavanja.Grad + ";" + manifestacija.MestoOdrzavanja.PostanskiBroj + ";" + manifestacija.BrojVip + ";" + manifestacija.BrojRegular + ";" + manifestacija.BrojFanpit + ";" + manifestacija.Deleted + ";" + manifestacija.UsernameProdavca + ";" + manifestacija.Slika;
            tw.WriteLine(str1 + ";" + str2);


            tw.Close();
        }
        public static void UpisiKartu(List<Karta>karte)
        {
            string putanja = HttpContext.Current.Server.MapPath(@"~/App_Data/karte.txt");
            TextWriter tw = new StreamWriter(putanja, false);

            foreach(Karta k in karte)
            {
                tw.WriteLine(k.Identifikator + ";" + k.Kupac.Username + ";" + k.NazivManifestacije + ";" + k.DatumManifestacije + ";" + k.Tip + ";" + k.Cena + ";" + k.StatusKarte + ";" + k.DatumRezervacije + ";" + k.Deleted + ";" + k.IdManifestacije);
            }



            tw.Close();
        }
        public static void UpisiKomentar(List<Komentar> komentari)
        {
            string putanja = HttpContext.Current.Server.MapPath(@"~/App_Data/komentari.txt");
            TextWriter tw = new StreamWriter(putanja, false);

            foreach (Komentar k in komentari)
            {
                tw.WriteLine(k.Username + ";" + k.IdManifestacije + ";" + k.TekstKomentara + ";" + k.Ocena+ ";" + k.Odobren);
            }



            tw.Close();
        }
        public static void UpisiKorisnike(List<Korisnik> korisnici)
        {
            string putanja = HttpContext.Current.Server.MapPath(@"~/App_Data/korisnici.txt");
            TextWriter tw = new StreamWriter(putanja, false);
            foreach (Korisnik korisnik in korisnici)
            {
                tw.WriteLine(korisnik.Username + ";" + korisnik.Password + ";" + korisnik.Ime + ";" + korisnik.Prezime + ";" + korisnik.Pol  + ";" + korisnik.DatumRodjenja + ";" + korisnik.Uloga + ";" + korisnik.Tip.ImeTipa + ";" + korisnik.Bodovi + ";" + korisnik.Deleted);

            }

            tw.Close();

        }

        public static void UpisiManifestacije(List<Manifestacija> manifestacije)
        {
            string putanja = HttpContext.Current.Server.MapPath(@"~/App_Data/manifestacije.txt");
            TextWriter tw = new StreamWriter(putanja, false);
            foreach (Manifestacija manifestacija in manifestacije)
            {
                string str1 = manifestacija.Id + ";" + manifestacija.Naziv + ";" + manifestacija.Tip + ";" + manifestacija.BrojMesta + ";" + manifestacija.DatumOdrzavanja + ";" + manifestacija.CenaRegularKarte + ";" + manifestacija.Status;

                string str2 = manifestacija.MestoOdrzavanja.Ulica + ";" + manifestacija.MestoOdrzavanja.BrojUlice + ";" + manifestacija.MestoOdrzavanja.Drzava + ";" + manifestacija.MestoOdrzavanja.Grad + ";" + manifestacija.MestoOdrzavanja.PostanskiBroj + ";" + manifestacija.BrojVip + ";" + manifestacija.BrojRegular + ";" + manifestacija.BrojFanpit + ";" + manifestacija.Deleted + ";" + manifestacija.UsernameProdavca + ";" + manifestacija.Slika;
                tw.WriteLine(str1 + ";" + str2);

            }

            tw.Close();

        }
    }
}