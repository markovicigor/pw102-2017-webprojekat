﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WebProjekat.Models
{
    public class Karta
    {
        public string Identifikator { get; set; }
        public Manifestacija Manifestacija { get; set; }
        public DateTime DatumManifestacije { get; set; }
        public string NazivManifestacije { get; set; }
        public double Cena { get; set; }
        public Korisnik Kupac { get; set; }
        public StatusKarte StatusKarte { get; set; }
        public TipKarte Tip { get; set; }
        public DateTime DatumRezervacije { get; set; }
        public bool Deleted { get; set; }
        public string IdManifestacije { get; set; }


        public Karta()
        {
            Identifikator = Guid.NewGuid().ToString();
        }
        public Karta(Manifestacija manifestacija, DateTime datumManifestacije, double cena, string kupac,StatusKarte status)
        {
            Kupac = new Korisnik();
            Kupac.Username = kupac;
            Identifikator = Guid.NewGuid().ToString();
            Manifestacija = manifestacija;
            //IdManifestacije = manifestacija.Id;
            DatumManifestacije = datumManifestacije;
            Cena = cena;
            StatusKarte = status;
            Deleted = false;

        }
        public Karta(string id,string username,string naziv,DateTime datumManifestacije,TipKarte tip, double cena,  StatusKarte status)
        {
            Kupac = new Korisnik();
            Manifestacija = new Manifestacija();
            Identifikator = Guid.NewGuid().ToString();
            NazivManifestacije = naziv;
            DatumManifestacije = datumManifestacije;
            Manifestacija.DatumOdrzavanja = datumManifestacije;
            Kupac.Username = username;
            Tip = tip;
            Cena = cena;

            StatusKarte = status;
            Deleted = false;



        }



    }
}