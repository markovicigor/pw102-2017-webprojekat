﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class Komentar
    {
        public Komentar()
        {
        }

        public Komentar(string username,string idManifestacije,string tekstKomentara, int ocena)
        {
            Username = username;
            IdManifestacije = idManifestacije;
            TekstKomentara = tekstKomentara;
            Ocena = ocena;
            Odobren = "NEODOBREN";
        }

        public Komentar(Korisnik kupac, Manifestacija manifestacija, string tekstKomentara, int ocena)
        {
            Kupac = kupac;
            Manifestacija = manifestacija;
            TekstKomentara = tekstKomentara;
            Ocena = ocena;
            Odobren = "NEODOBREN";
        }

        public Korisnik Kupac { get; set; }
        public Manifestacija Manifestacija { get; set; }
        public string TekstKomentara { get; set; }
        public int Ocena { get; set; }
        public string Username { get; set; }
        public string IdManifestacije { get; set; }
        public string Odobren { get; set; }
    }
}