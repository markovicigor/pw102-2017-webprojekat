﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class Korisnik
    {
        public Korisnik()
        {
            Tip = new TipKorisnika();
            Tip.ImeTipa = "Bronzani";
            Bodovi = 0;
            Deleted = false;
        }

        public Korisnik(string username, string password, string ime, string prezime, Pol pol, DateTime datumRodjenja, Uloga uloga,string imeTipa,double bodovi)
        {
            Username = username;
            Password = password;
            Ime = ime;
            Prezime = prezime;
            Pol = pol;
            DatumRodjenja = datumRodjenja;
            Uloga = Uloga.KUPAC;

            Manifestacija = new Manifestacija();
            Tip = new TipKorisnika();
            Tip.ImeTipa = imeTipa;
            Bodovi = bodovi;
            Deleted = false;

        }

        public string Username { get; set; }
        public string Password { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public Pol Pol { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public Uloga Uloga { get; set; }
        public Karta RezervisaneKarte { get; set; }
        public Manifestacija Manifestacija { get; set; }
        public TipKorisnika Tip { get; set; }
        public double Bodovi { get; set; }
        public bool Deleted { get; set; }
        public bool LoggedIn { get; set; }

    }
}