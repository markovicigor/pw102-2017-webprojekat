﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class Lokacija
    {
        public string  GeogSirina { get; set; }
        public string  GeogDuzina { get; set; }
        public MestoOdrzavanja MestoOdrzavanja { get; set; }

    }
}