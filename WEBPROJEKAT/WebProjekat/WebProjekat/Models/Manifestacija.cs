﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class Manifestacija
    {
        public Manifestacija()
        {
        }

        public Manifestacija(string id,string naziv, TipManifestacije tip, int brojMesta, DateTime datumOdrzavanja, double cenaRegularKarte, StatusManifestacije status)
        {
            Komentar = new Komentar();
            Id =  id;
            Naziv = naziv;
            Tip = tip;
            BrojMesta = brojMesta;
            DatumOdrzavanja = datumOdrzavanja;
            CenaRegularKarte = cenaRegularKarte;
            Status = StatusManifestacije.NEAKTIVNO;
            MestoOdrzavanja = new MestoOdrzavanja();
            Deleted = false;

            Komentar.TekstKomentara = "/";
           // Komentar.Ocena = 0;
        }
        public Manifestacija(string id, string naziv, TipManifestacije tip, int brojMesta, DateTime datumOdrzavanja, double cenaRegularKarte,string ulica,int brojUlice,string drzava,string grad,int postanskiBroj,int brojVip,int brojRegular,int brojFanpit,string usernameProdavca,string slika)
        {
            Komentar = new Komentar();
            Id = id;
            Naziv = naziv;
            Tip = tip;
            BrojVip = brojVip;
            BrojRegular = brojRegular;
            BrojFanpit = brojFanpit;
            BrojMesta = brojVip + brojRegular + brojFanpit;
            DatumOdrzavanja = datumOdrzavanja;
            CenaRegularKarte = cenaRegularKarte;
            Status = StatusManifestacije.NEAKTIVNO;
            MestoOdrzavanja = new MestoOdrzavanja(ulica,brojUlice,drzava,grad,postanskiBroj);
            Deleted = false;
            UsernameProdavca = usernameProdavca;
            Slika = slika;
            Komentar.TekstKomentara = "/";
            Komentari = new List<Komentar>();


        }

        public string Id { get; set; }
        public string Naziv { get; set; }
        public TipManifestacije Tip { get; set; }
        public int BrojMesta { get; set; }
        public int BrojVip { get; set; }
        public int BrojRegular { get; set; }
        public int BrojFanpit { get; set; }
        public DateTime DatumOdrzavanja { get; set; }
        public double CenaRegularKarte { get; set; }
        public StatusManifestacije Status { get; set; }
        public MestoOdrzavanja MestoOdrzavanja { get; set; }
        public string Slika { get; set; }
        public bool Deleted { get; set; }
        public string UsernameProdavca { get; set; }
        public Komentar Komentar { get; set; }
        public List<Komentar> Komentari { get; set; }

    }
}