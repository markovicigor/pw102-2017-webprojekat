﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class MestoOdrzavanja
    {
        public MestoOdrzavanja()
        {

        }
        public MestoOdrzavanja(string ulica, int brojUlice,string drzava, string grad, int postanskiBroj)
        {
            Ulica = ulica;
            BrojUlice = brojUlice;
            Drzava = drzava;
            Grad = grad;
            PostanskiBroj = postanskiBroj;
        }

        public string Ulica { get; set; }
        public int BrojUlice { get; set; }
        public string Drzava { get; set; }
        public string Grad { get; set; }
        public int PostanskiBroj { get; set; }
    }
}