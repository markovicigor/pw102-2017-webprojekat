﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class TipKorisnika
    {
        public TipKorisnika()
        {

        }

        public TipKorisnika(string imeTipa, double bodovi)
        {
            ImeTipa = imeTipa;
            Bodovi = bodovi;
        }

        public string ImeTipa { get; set; }
        public double Popust { get; set; }
        public double Bodovi { get; set; }

        public string KojiJeTip(double bodovi)
        {

            if(bodovi >= 3000 && bodovi < 4000)
            {
                ImeTipa = "Srebrni";

                return ImeTipa;
            }
            else if(bodovi >= 4000)
            {
                ImeTipa = "Zlatni";

                return ImeTipa;
            }
            else
            {
                ImeTipa = "Bronzani";

                return ImeTipa;
            }
        }
    }
}